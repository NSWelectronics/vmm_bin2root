# vmm_bin2root

This package has the necessary code to convert the binary data files
produced by [vmm_readout_software](https://gitlab.cern.ch/NSWelectronics/vmm_readout_software)
into ROOT ntuples of the same format as the ROOT ntuples produced by
[vmm_readout_software](https://gitlab.cern.ch/NSWelectronics/vmm_readout_software).

## Requirements
The only requirements are a valid C++ compiler and ROOT.

## Preparation/Installation
Checkout this package:

```
git clone https://:@gitlab.cern.ch:8443/NSWelectronics/vmm_bin2root.git
```
and simply compile:

```
cd vmm_bin2root/
make
```

This will create the executable **bin2root_main** located under **bin/**, inspect the options that this executable takes by calling the help prompt:

```
./bin/bin2root_main --help
----------------------------------------------------------------
 * vmm_bin2root *

 Usage: bin2root_main -i <input-bin-file> [options]

 Options:
  -i|--input           input binary file for conversion (required)
  -d|--dbg             set debug level (default: 0, higher integer for more verbosity)
  -h|--help            print this help message

----------------------------------------------------------------
```

It may be useful to add this executable directory to your path, so that you can simply call **bin2root_main** from any location. This is useful if
you are creating output binary files in a location that is not convenient for you to call the executable via its full path. Add the executable
to your PATH environment variable by calling the following command in a terminal/shell:

```
export PATH=<parent-path>/vmm_bin2root/bin/:${PATH}
```


## How to convert your binary data files into ROOT ntuples

Assuming an input file **run_1234.bin** as your raw binary input file:

```
bin2root_main -i run_1234.bin
```

This will create an output ROOT file **in the same directory that you are in** called "run_1234.root". There are no checks for pre-existing files of the same name. Files may be overwritten.


