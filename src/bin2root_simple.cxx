#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
using namespace std;


// BIT MASKS
const uint32_t IP0 = 0xff000000;
const uint32_t IP1 = 0xff0000;
const uint32_t IP2 = 0xff00;
const uint32_t IP3 = 0xff;
const uint32_t TRAILER = 0xffffffff;


void help()
{
    cout << "--------------------------------------------------------" << endl;
    cout << " * bin2root_simple * " << endl;
    cout << endl;
    cout << " Usage: bin2root_simple -i <input-bin-file> [options]" << endl;
    cout << endl;
    cout << " Options: " << endl;
    cout << "  -i|--input           input binary file for reading (required)" << endl;
    cout << "  -d|--dbg             set debug level (default: 0, higher integer for more verbosity)" << endl;
    cout << "  -n|--nevents         number of events to process (default: 20, -1 for all)" << endl;
    cout << "  --l0                 decode L0-type data (default: false)" << endl;
    cout << "  -h|--help            print this help message" << endl;
    cout << endl;
    cout << "--------------------------------------------------------" << endl;

    return;
}


uint32_t correct_ordering(const uint32_t& x_) {
    uint32_t x = 0;
    x = (0xff & x_) << 24; 
    x |= ((x_ >> 8) & 0xff) << 16;
    x |= ((x_ >> 16) & 0xff) << 8;
    x |= ((x_ >> 24) & 0xff) << 0;
    return x;

}

int main(int argc, char* argv[])
{

    // read in the command line arguments
    string input_file = "";
    int dbg = 0;
    int nevents = 20;
    bool do_l0 = false;

    int optin(1);
    while(optin < argc) {
        string in = argv[optin];
        if      (in == "-i" || in == "--input" ) { input_file = argv[++optin]; }
        else if (in == "-d" || in == "--dbg" )   { dbg = atoi(argv[++optin]); }
        else if (in == "-n" || in == "--nevents"){ nevents = atoi(argv[++optin]); }
        else if (in == "--l0")                   { do_l0 = true; }
        else if (in == "-h" || in == "--help")   { help(); return 0; }
        else {
            cout << "bin2root_simple    Unknown command line argument provided '"
                    << in << "'" << endl;
            help();
            return 1;
        }
        optin++;
    }

    // input file is required
    if(input_file=="") {
        cout << " no input file provided! " << endl;
        help();
        return 1;
    }

    cout << "\n reading input file: " << input_file << "\n" <<  endl;
    bool file_found = std::ifstream(input_file).good();
    if(!file_found) {
        cout << "\n input file '" << input_file << "' not found!\n" << endl;
        return 1;
    }

    ///////////////////////////////////////////////////////////////////////////
    // open the input file and load as binary
    ///////////////////////////////////////////////////////////////////////////
    ifstream infile;
    infile.open(input_file.c_str(), std::ifstream::in | std::ifstream::binary);
    if(!infile) {
        cout << "\n error opening the input binary file!\n" << endl;
        return 1;
    }

    
    ///////////////////////////////////////////////////////////////////////////
    // read in the run properties
    ///////////////////////////////////////////////////////////////////////////

    uint32_t run_number = 0;
    uint32_t gain = 0;
    uint32_t tac_slope = 0;
    uint32_t peak_time = 0;
    uint32_t threshold_dac = 0;
    uint32_t pulser_dac = 0;
    uint32_t detector_angle = 0;
    uint32_t tp_skew = 0;

    infile.read((char*)(&run_number), sizeof(uint32_t));
    infile.read((char*)(&gain), sizeof(uint32_t));
    infile.read((char*)(&tac_slope), sizeof(uint32_t));
    infile.read((char*)(&peak_time), sizeof(uint32_t));
    infile.read((char*)(&threshold_dac), sizeof(uint32_t));
    infile.read((char*)(&pulser_dac), sizeof(uint32_t));
    infile.read((char*)(&detector_angle), sizeof(uint32_t));
    infile.read((char*)(&tp_skew), sizeof(uint32_t));

    cout << "------------------ run properties ------------------------" << endl;
    cout << "  run number           : " << run_number << endl;
    cout << "  gain idx             : " << gain << endl;
    cout << "  tac slope            : " << tac_slope << endl;
    cout << "  peak time            : " << peak_time << endl;
    cout << "  threshold DAC        : " << threshold_dac << endl;
    cout << "  pulser DAC           : " << pulser_dac << endl;
    cout << "  detector angle       : " << detector_angle << endl;
    cout << "  tp skew              : " << tp_skew << endl;
    cout << "----------------------------------------------------------" << endl;
    

    ///////////////////////////////////////////////////////////////////////////
    // begin reading in the events
    ///////////////////////////////////////////////////////////////////////////
    uint32_t event_number = -1;

    while(true) {

        event_number++;
        if( !(nevents<0) && (event_number >= nevents) ) break;

        // board ip
        uint32_t board_ip = 0;
        infile.read((char*)(&board_ip), sizeof(uint32_t));
        if(!infile) {
            break;
        }

        uint32_t ip0 = (IP0 & board_ip) >> 24;
        uint32_t ip1 = (IP1 & board_ip) >> 16;
        uint32_t ip2 = (IP2 & board_ip) >> 8;
        uint32_t ip3 = (IP3 & board_ip);
        stringstream ip;
        ip << ip0 << "." << ip1 << "." << ip2 << "." << ip3;

        if(dbg) {
            cout << " - - - - - - - - - - - - - - - - - - - - - - - - - - - - - " << endl;
            cout << " * event " << event_number << " from board with IP: " << ip.str() << " * " << endl; 
        } 


        // ---------------------------------------------------------------- //
        //  collect the hit data into a vector
        // ---------------------------------------------------------------- //
        vector<uint32_t> data;
        uint32_t chunk = 0;
        while(chunk != TRAILER) {
            infile.read((char*)(&chunk), sizeof(uint32_t));
            chunk = correct_ordering(chunk);
            data.push_back(chunk);
        } // while
        if(!infile.good()) break;

        // ---------------------------------------------------------------- //
        //  decode the VMM event header
        // ---------------------------------------------------------------- //
        uint32_t header0 = data.at(0);
        uint32_t header1 = data.at(1);

        uint32_t trigger_counter = 0;
        uint32_t chip_number = 0;
        uint32_t orbit_count = 0;
        uint32_t bcid_l0 = 0;

        if(!do_l0) {
            trigger_counter = header0;
            chip_number = (header1 & 0xff);
        }
        else {
            trigger_counter = header0;
            chip_number = (header1 & 0xff000000) >> 24;
            orbit_count = (header1 & 0x300) >> 12; 
            bcid_l0 = (header1 & 0xfff);
        }

        //uint32_t trigger_counter = (header0);
        //uint32_t chip_number = (header1 & 0xff);

        if(dbg) {
            cout << " > VMM ID: " << chip_number << "  trigger #" << trigger_counter << endl;
        }

        // if we are at the trailer, go to next event
        if(header0 == TRAILER) continue;

        // ---------------------------------------------------------------- //
        //  decode the VMM event data
        // ---------------------------------------------------------------- //
        int hit_size = (do_l0 ? 1 : 2);
        for(int i = 2; i < (int)data.size(); i+=hit_size) {

            uint32_t d0 = data.at(i);
            if(d0==TRAILER) break; // done reading this hit

            if(!do_l0) {

                uint32_t d1 = data.at(i+1);

                uint32_t pdo;
                uint32_t tdo;
                uint32_t graycode;
                uint32_t flag;
                uint32_t vmm_channel;
                uint32_t threshold_passed;

                pdo = (d0 & 0x3ff);
                tdo = (d0 & 0x3fc00000) >> 22;
                graycode = (d0 & 0x3ffc00) >> 10;
                flag = (d1 & 0x1);
                vmm_channel = (d1 & 0xfc) >> 2;
                threshold_passed = (d1 & 0x2) >> 1;

                if(dbg >= 5) {
                    cout << "    vmm channel: " << vmm_channel
                             << "  pdo: " << pdo << "  tdo: " << tdo
                             << "  gray: " << graycode
                             << "  flag: " << flag
                             << "  threshold passed: " << threshold_passed << endl;
                }
            } // do non-L0 decoding

            else if(do_l0) {

                uint32_t pdo;
                uint32_t tdo;
                uint32_t vmm_channel;
                uint32_t relbcid;

                pdo = (d0 & 0x3ff000) >> 12;
                tdo = (d0 & 0xff0) >> 4;
                vmm_channel = (d0 & 0xfc00000) >> 22;
                relbcid = (d0 & 0x7);

                if(dbg >= 5) {
                    cout << "   vmm channel: " << vmm_channel
                            << "  pdo: " << pdo << "  tdo: " << tdo
                            << "  relative bcid: " << relbcid << endl;
                }

            } // doing L0 decoding
        }
    } // while


    return 0;
}
