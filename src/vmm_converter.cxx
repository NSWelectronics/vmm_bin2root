#include "vmm_converter.hh"

//std/stl
#include <iostream>
#include <iomanip> // setprecision
#include <math.h> // pow
#include <sstream>
using namespace std;

//vmm
#include "bit_manip.hh"
#include "vmm_hit.hh"
#include "trigger_event.hh"


///////////////////////////////////////////////////////////////////////////////
VMMConverter::VMMConverter() :
    m_dbg(0),
    m_do_l0_decoding(false),
    m_do_plots(false),
    n_trig_count(0)
{

    m_decoder.clear(true);
    m_decoder.set_type(VMMDecoder::VMMEVENT);

    m_trigger_events.clear();

}
///////////////////////////////////////////////////////////////////////////////
void VMMConverter::do_l0_decoding(bool doit)
{
    if(doit) {
        cout << "VMMConverter::do_l0_decoding    Decoding type: VMMEVENTL0" << endl;
        m_decoder.set_type(VMMDecoder::VMMEVENTL0);
        m_do_l0_decoding = true;
    }
    else {
        cout << "VMMConverter::do_l0_decoding    Decoding type: VMMEVENT" << endl;
        m_decoder.set_type(VMMDecoder::VMMEVENT);
        m_do_l0_decoding = false;
    }
}
///////////////////////////////////////////////////////////////////////////////
bool VMMConverter::load_file(string filename)
{

    cout << "VMMConverter::load_file    Loading input file: " << filename << endl;

    //std::ifstream ifs ( filename.c_str(), std::ifstream::in | std::ifstream::binary );
    m_ifile.open(filename.c_str(), std::ifstream::in | std::ifstream::binary);

    if(!m_ifile) {
        cout << "VMMConverter::load_file    Unable to open input file for reading!" << endl;
        return false;
    }

    m_in_filename = filename;

    // file opened ok, now extract the run properties


    //uint32_t run_number;
    m_ifile.read((char*)(&m_run_number), sizeof(uint32_t));

    if(dbg())
    cout << "VMMConverter::load_file    Binary data file for run number " << m_run_number
                << " opened successfully for reading" << endl;

    stringstream outnumber;
    outnumber << std::setw( 4 ) << std::setfill( '0' ) << m_run_number;
    stringstream outfilename;
    outfilename << "run_" << outnumber.str() << ".root";
    if(dbg()) {
        cout << "VMMConverter::load_file    Setting output filename to: " << outfilename.str() << endl;
    }
    m_out_filename = outfilename.str();

    // setup the output ROOT file and TTrees
    setup_output_file();

    // fill the run properties data and tree
    get_run_properties();

    return true;
}
///////////////////////////////////////////////////////////////////////////////
void VMMConverter::clear_data()
{
    m_gain = 0;
    m_tac = 0;
    m_peak_time = 0;
    m_threshold_dac = 0;
    m_pulser_dac = 0;
    m_angle = 0;
    m_tp_skew = 0;

    m_pdo.clear();
    m_tdo.clear();
    m_flag.clear();
    m_threshold.clear();
    m_bcid.clear();
    m_gray_decoded.clear();
    m_channel.clear();
}
///////////////////////////////////////////////////////////////////////////////
void VMMConverter::setup_output_file()
{

    // create file
    m_root_file = new TFile(m_out_filename.c_str(), "UPDATE");
    if(m_root_file->IsZombie()) {
        cout << "VMMConverter::setup_output_file    ERROR ROOT file '" << m_out_filename
                << "' unable to be created. Exiting." << endl;
        delete m_root_file;
        exit(1);
    }
    m_root_file->cd();

    // run properties tree
    m_run_properties_tree = new TTree("run_properties", "run_properties");
    br_run_number       = m_run_properties_tree->Branch("runNumber", &m_run_number);
    br_gain             = m_run_properties_tree->Branch("gain", &m_gain);
    br_tac              = m_run_properties_tree->Branch("tacSlope", &m_tac);
    br_peak_time        = m_run_properties_tree->Branch("peakTime", &m_peak_time);
    br_threshold_dac    = m_run_properties_tree->Branch("dacCounts", &m_threshold_dac);
    br_pulser_dac       = m_run_properties_tree->Branch("pulserCounts", &m_pulser_dac);
    br_angle            = m_run_properties_tree->Branch("angle", &m_angle);
    br_tp_skew          = m_run_properties_tree->Branch("tpSkew", &m_tp_skew);

    // vmm tree
    m_vmm_tree = new TTree("vmm", "vmm");
    br_trigger_timestamp        = m_vmm_tree->Branch("triggerTimeStamp", "std::vector<int>", &m_trigger_timestamp);
    br_trigger_counter          = m_vmm_tree->Branch("triggerCounter", "std::vector<int>", &m_trigger_counter);
    br_board_id                 = m_vmm_tree->Branch("boardId", "std::vector<int>", &m_board_id);
    br_chip_id                  = m_vmm_tree->Branch("chip", "std::vector<int>", &m_chip);
    br_event_size               = m_vmm_tree->Branch("eventSize", "std::vector<int>", &m_event_size);
    br_tdo                      = m_vmm_tree->Branch("tdo", "std::vector< vector<int> >", &m_tdo);
    br_pdo                      = m_vmm_tree->Branch("pdo", "std::vector< vector<int> >", &m_pdo);
    br_flag                     = m_vmm_tree->Branch("flag", "std::vector< vector<int> >", &m_flag);
    br_threshold                = m_vmm_tree->Branch("threshold", "std::vector< vector<int> >", &m_threshold);
    br_bcid                     = m_vmm_tree->Branch("bcid", "std::vector< vector<int> >", &m_bcid);
    br_gray_decoded             = m_vmm_tree->Branch("grayDecoded", "std::vector< vector<int> >", &m_gray_decoded);
    br_channel                  = m_vmm_tree->Branch("channel", "std::vector< vector<int> >", &m_channel);
    if(do_l0()) {
        br_relbcid                  = m_vmm_tree->Branch("relbcid", "std::vector< vector<int> >", &m_relbcid);
        br_overflow                 = m_vmm_tree->Branch("overflow", "std::vector< vector<int> >", &m_overflow);
        br_orbit_count              = m_vmm_tree->Branch("orbitCount", "std::vector< vector<int> >", &m_orbit_count);
    }
    
    


}
///////////////////////////////////////////////////////////////////////////////
float VMMConverter::float_from_int(uint32_t input)
{
    uint32_t sign_mask = 0x80000000; // 0 for positive, 1 for negative
    uint32_t e_mask = 0x7f800000;
    uint32_t f_mask = 0x7fffff;

    uint32_t sign = (input & sign_mask) >> 31;
    uint32_t e = (input & e_mask) >> 23;
    uint32_t f = (input & f_mask);

    //cout << "input: " << bitset<32>(input) << endl;
    //cout << "sign: " << bitset<1>(sign) << "  e: " << bitset<8>(e) << "  f: " << bitset<23>(f) << endl;

    int bias = 127;

    double x = (1 - 2 * sign) * (1 + f) * pow(2, ( e - bias ));
    //cout << "  --> float: " << x << endl;

    return x;

    
}
///////////////////////////////////////////////////////////////////////////////
void VMMConverter::get_run_properties()
{
    // have already extracted the run number at this point

    uint32_t gain;
    m_ifile.read((char*)(&gain), sizeof(uint32_t));

    uint32_t tac;
    m_ifile.read((char*)(&tac), sizeof(uint32_t));

    uint32_t peak_time;
    m_ifile.read((char*)(&peak_time), sizeof(uint32_t));

    uint32_t threshold_dac;
    m_ifile.read((char*)(&threshold_dac), sizeof(uint32_t));

    uint32_t pulser_dac;
    m_ifile.read((char*)(&pulser_dac), sizeof(uint32_t));

    uint32_t angle;
    m_ifile.read((char*)(&angle), sizeof(uint32_t));

    uint32_t tp_skew;
    m_ifile.read((char*)(&tp_skew), sizeof(uint32_t));
    tp_skew = float_from_int(tp_skew);

    if(dbg()>=5) {
        cout << "VMMConverter::get_run_properties    Run properties extracted: " << endl;
        cout << "   > gain:             " << gain << endl;
        cout << "   > tac:              " << tac << endl;
        cout << "   > peak time         " << peak_time << endl;
        cout << "   > threshold DAC     " << threshold_dac << endl;
        cout << "   > pulser DAC        " << pulser_dac << endl;
        cout << "   > angle             " << angle << endl;
        cout << "   > tp skew           " << tp_skew << endl;
    }

    m_gain = gain;
    m_tac = tac;
    m_peak_time = peak_time;
    m_threshold_dac = threshold_dac;
    m_pulser_dac = pulser_dac;
    m_angle = angle;
    m_tp_skew = tp_skew;

    // fill the run properties ttree
    m_root_file->cd();
    m_run_properties_tree->Fill();

    return;
}
///////////////////////////////////////////////////////////////////////////////
void VMMConverter::convert()
{

    // assume we have already processed the run properties

    int test = 0;
    while(m_ifile.good()) {

        if(!get_hit_data()) { flush(); break; }
        //if(test>10)
        //break;
        //test++;

        //uint32_t chunk;
        //m_ifile.read((char*)(&chunk), sizeof(uint32_t));

        //uint32_t




        //if(m_ifile.good()) {
        //    incoming.push_back(chunk);
        //    if(chunk==0xffffffff)
        //}
    }

    // done looping over the input, save the output file
    cout << "VMMConverter::convert    Converted data for " << n_trig_count << " triggers "
            << "worth of data" << endl;
    write_output();


}
///////////////////////////////////////////////////////////////////////////////
void VMMConverter::write_output()
{
    m_root_file->cd();
    m_vmm_tree->Write("", TObject::kOverwrite);
    if(!m_root_file->Write()) {
        cout << "VMMConverter::write_output    ERROR Writing output ROOT file!" << endl;
    }
    m_root_file->Close();

    cout << "VMMConverter::write_output    ROOT file written to: " << m_out_filename << endl;

}
///////////////////////////////////////////////////////////////////////////////
bool VMMConverter::get_hit_data()
{

    if(m_trigger_events.ready_to_read()) {
        fill_trigger_event();
    }

    // get the board IP & ID
    uint32_t board_ip;
    m_ifile.read((char*)(&board_ip), sizeof(uint32_t));
    if(!m_ifile.good()) return false;

    uint32_t m0 = 0xff000000;
    uint32_t m1 = 0xff0000;
    uint32_t m2 = 0xff00;
    uint32_t m3 = 0xff;
    uint32_t ip0 = (m0 & board_ip) >> 24;
    uint32_t ip1 = (m1 & board_ip) >> 16;
    uint32_t ip2 = (m2 & board_ip) >> 8;
    uint32_t ip3 = (m3 & board_ip);
    stringstream ip;
    ip << ip0 << "." << ip1 << "." << ip2 << "." << ip3;

    uint32_t board_id = ip3;

    if(dbg()>=10) {
        cout << "VMMConverter::get_hit_data    Data from board with IP: " << ip.str() << endl;
    }


    // gather the relevant hit data
    vector<uint32_t> data;
    uint32_t chunk = 0;
    while(chunk != 0xffffffff) {
        m_ifile.read((char*)(&chunk), sizeof(uint32_t));
        chunk = bits::reverse_octet_order()(chunk);
        data.push_back(chunk);
    }
    if(!m_ifile.good()) return false;

    // decode header (header is 2x32 bits for both L0 and non-L0 decoding)
    bool continue_decoding = m_decoder.decode(vector<uint32_t>(data.begin(), data.begin()+3), true);

    // hit size is 1x32-bit for L0 and 2x32-bit for non-L0
    int hit_size = (do_l0() ? 1 : 2);

    while(true) {

        if(!continue_decoding)
            break;

        uint32_t trigger_count = data.at(0);

        VMMHit vmm_hit;
        for(unsigned int i = 3; i < data.size(); i+=hit_size) {

            continue_decoding = m_decoder.decode(vector<uint32_t>(data.begin() + i, data.begin() + i + hit_size), false);
            if(!continue_decoding) {
                break;
            }
            vmm_hit.clear();
            vmm_hit.set_vmm_channel(m_decoder.vmm_channel());
            // pdo
            vmm_hit.set_pdo(m_decoder.pdo());
            // tdo
            vmm_hit.set_tdo(m_decoder.tdo());
            // gray code
            vmm_hit.set_graycode_bcid(m_decoder.bcid());
            // decoded
            vmm_hit.set_decoded_graycode_bcid(m_decoder.decoded_bcid());

            if(!do_l0()) {
                // flag
                vmm_hit.set_flag(m_decoder.flag());
                // signal passes threshold
                vmm_hit.set_pass_threshold(m_decoder.pass_threshold());
            }

            if(do_l0()) {

                // relative bcid
                vmm_hit.set_relative_bcid(m_decoder.relbcid());
                // vmm overflow bit
                vmm_hit.set_overflow_bit(m_decoder.vmm_overflow());
                // orbit counter
                vmm_hit.set_orbit_counter(m_decoder.orbit_counter());

            }

            // board id
            vmm_hit.set_board_id(board_id);

            int32_t chip_no = m_decoder.chip_number();
            //cout << "trig: " << trigger_count << "  chip: " << chip_no << " channel: " << m_decoder.vmm_channel() << "  pdo: " << m_decoder.pdo() << "  tdo: " << m_decoder.tdo() << "  relbcid: " << m_decoder.relbcid() << "  orbit counter: " << m_decoder.orbit_counter() << endl;
            m_trigger_events.add_hit2(trigger_count, board_id, chip_no, vmm_hit);

        } // i

    } // while

    return true;

}
///////////////////////////////////////////////////////////////////////////////
void VMMConverter::fill_trigger_event()
{

    bool trig_ok = true;
    uint32_t trigger_to_readout = m_trigger_events.trigger_to_read(trig_ok);
    if(!trig_ok) return;

    std::map<BoardChipPair, std::vector<VMMHit> > hitgroup = m_trigger_events.get_trigger2(trigger_to_readout);
    if(hitgroup.size()==0) {
        cout << "VMMConverter::fill_trigger_event    WARNING Hit group empty for "
            << "trigger " << trigger_to_readout << " (0x" << std::hex
            << trigger_to_readout << std::dec << ")" << endl;
        return;
    }

    // increment count
    n_trig_count++;

    // contianers to store

    vector<int> board_id_tree;
    vector<int> chip_id_tree;
    vector<int> eventsize_tree;
    vector<int> trigcounter_tree;

    vector< vector<int> > vmm_channel_tree;
    vector< vector<int> > pdo_tree;
    vector< vector<int> > tdo_tree;
    vector< vector<int> > flag_tree;
    vector< vector<int> > threshold_tree;
    vector< vector<int> > bcid_tree;
    vector< vector<int> > graydecoded_tree;
    vector< vector<int> > relbcid_tree;
    vector< vector<int> > overflow_tree;
    vector< vector<int> > orbit_count_tree;

    vector<int> vmm_channel;
    vector<int> pdo;
    vector<int> tdo;
    vector<int> flag;
    vector<int> threshold;
    vector<int> bcid;
    vector<int> graydecoded;
    vector<int> relbcid;
    vector<int> overflow;
    vector<int> orbit_count;

    size_t n_bytes_read = 0;
    int hit_size = (do_l0() ? 1 : 2);
    for(const auto group : hitgroup) {
        BoardChipPair bcpair = group.first;

        vmm_channel.clear();
        pdo.clear();
        tdo.clear();
        flag.clear();
        threshold.clear();
        bcid.clear();
        graydecoded.clear();
        relbcid.clear();
        overflow.clear();
        orbit_count.clear();

        vector<VMMHit> hits = hitgroup[bcpair];
        for(auto hit : hits) {
            vmm_channel.push_back(hit.get_vmm_channel());
            pdo.push_back(hit.get_pdo());
            tdo.push_back(hit.get_tdo());
            if(!do_l0()) {
                flag.push_back(hit.get_flag());
                threshold.push_back(hit.get_pass_threshold());
            }
            bcid.push_back(hit.get_graycode_bcid());
            graydecoded.push_back(hit.get_decoded_graycode_bcid());

            if(do_l0()) {
                relbcid.push_back(hit.get_relative_bcid());
                overflow.push_back(hit.get_overflow_bit());
                orbit_count.push_back(hit.get_orbit_counter());
            }

            n_bytes_read += hit_size*4;
        } // hit

        if(n_bytes_read>0) {
            trigcounter_tree.push_back(trigger_to_readout);
            board_id_tree.push_back(bcpair.first);
            chip_id_tree.push_back(bcpair.second);
            eventsize_tree.push_back(n_bytes_read);

            vmm_channel_tree.push_back(vmm_channel);
            pdo_tree.push_back(pdo);
            tdo_tree.push_back(tdo);
            if(!do_l0()) {
                flag_tree.push_back(flag);
                threshold_tree.push_back(threshold);
            }
            if(do_l0()) {
                relbcid_tree.push_back(relbcid);
                overflow_tree.push_back(overflow);
                orbit_count_tree.push_back(orbit_count);
            }
            bcid_tree.push_back(bcid);
            graydecoded_tree.push_back(graydecoded);
        }

    } // group

    if(n_bytes_read>0 && chip_id_tree.size()) {

        // clear output tree containers

        m_board_id.clear();
        m_trigger_counter.clear();
        m_chip.clear();
        m_event_size.clear();

        m_pdo.clear();
        m_tdo.clear();
        m_flag.clear();
        m_threshold.clear();
        m_bcid.clear();
        m_gray_decoded.clear();
        m_channel.clear();
        m_relbcid.clear();
        m_overflow.clear();
        m_orbit_count.clear();

        // link to output tree containers

        m_board_id = board_id_tree;
        m_trigger_counter = trigcounter_tree;
        m_chip = chip_id_tree;
        m_event_size = eventsize_tree;

        m_pdo = pdo_tree;
        m_tdo = tdo_tree;
        m_flag = flag_tree;
        m_threshold = threshold_tree;
        m_bcid = bcid_tree;
        m_gray_decoded = graydecoded_tree;
        m_channel = vmm_channel_tree; 

        m_relbcid = relbcid_tree;
        m_overflow = overflow_tree;
        m_orbit_count = orbit_count_tree;

        // fill the output ntuples
        fill_event();
    }

    // clear the readout trigger
    m_trigger_events.remove_trigger(trigger_to_readout);
}
///////////////////////////////////////////////////////////////////////////////
void VMMConverter::flush()
{
    for(int i = 0; i < (int)m_trigger_events.capacity(); i++) {
        fill_trigger_event();
    }
}
///////////////////////////////////////////////////////////////////////////////
void VMMConverter::fill_event()
{

    m_root_file->cd();
    m_vmm_tree->Fill();

}
