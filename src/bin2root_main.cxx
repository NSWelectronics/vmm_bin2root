#include <iostream>
#include <string>
#include <fstream>
using namespace std;

// vmm
#include "vmm_converter.hh"

void help()
{
    cout << "----------------------------------------------------------------" << endl;
    cout << " * vmm_bin2root * " << endl;
    cout << endl;
    cout << " Usage: bin2root_main -i <input-bin-file> [options]" << endl;
    cout << endl;
    cout << " Options:" << endl;
    cout << "  -i|--input           input binary file for conversion (required)" << endl;
    //cout << "  -p|--plots           make some diagnostic/summary plots (default: false)" << endl;
    cout << "  -d|--dbg             set debug level (default: 0, higher integer for more verbosity)" << endl;
    cout << "  --l0                 perform L0 decoding (default: false)" << endl;
    cout << "  -h|--help            print this help message" << endl;
    cout << endl;
    cout << "----------------------------------------------------------------" << endl;

    return;
}

int main(int argc, char* argv[])
{

    string input_file = "";
    bool make_plots = false;
    bool do_l0 = false;
    int dbg = 0;

    int optin(1);
    while(optin < argc) {
        string in = argv[optin];
        if      (in == "-i" || in == "--input"  ) { input_file = argv[++optin]; }
        //else if (in == "-p" || in == "--plots"  ) { make_plots = true; }
        else if (in == "-d" || in == "--dbg"    ) { dbg = atoi(argv[++optin]); }
        else if (in == "-h" || in == "--help"   ) { help(); return 0; }
        else if (in == "--l0" )                   { do_l0 = true; }
        else {
            cout << "bin2root_main    Unknown command line argument provided '" << in << "'" << endl;
            help();
            return 1;
        }
        optin++;
    }// while

    // input file is required
    if(input_file=="") {
        cout << "\nbin2root_main    ERROR You did not provide an input file for conversion!" << endl;
        cout << "bin2root_main    ERROR Printing usage and exiting.\n" << endl; 
        help();
        return 1;
    }

    cout << "-------------------------------------------------------" << endl;
    cout << " * vmm_bin2root * " << endl;
    cout << endl;
    cout << "  file for conversion:     " << input_file << endl;
    //cout << "  make plots?:             " << make_plots << endl;
    cout << "  debug level:             " << dbg << endl;
    cout << endl;
    cout << "-------------------------------------------------------" << endl;

    // check that file exists
    bool file_found = std::ifstream(input_file).good();
    if(!file_found) {
        cout << "bin2root_main    ERROR Input file '" << input_file << "' cannot be found, exiting" << endl;
        return 1;
    }

    VMMConverter* converter = new VMMConverter();
    converter->set_debug(dbg);
    converter->set_plotting(make_plots);
    converter->do_l0_decoding(do_l0);
    if(!converter->load_file(input_file)) {
        return 1;
    }
    converter->convert();

    cout << endl;
    cout << "bin2root_main    Binary data file converted to ROOT file. Goodbye!" << endl;
    cout << endl;




    return 0;
}
