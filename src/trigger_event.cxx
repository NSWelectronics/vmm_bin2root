#include "trigger_event.hh"

// std/stl
#include <iostream>
#include <algorithm> // sort, find
using namespace std;

TriggerEvent::TriggerEvent() :
    m_capacity(10),
    n_warning(0)
{
    clear();
}

/////////////////////////////////////////////////////////////////////////////
void TriggerEvent::clear()
{
    m_trigger_map.clear();
    m_trigger_map2.clear();
    m_triggers.clear();
}
/////////////////////////////////////////////////////////////////////////////
bool TriggerEvent::ready_to_read()
{
    // this may need to be thought about a bit more
    return (m_triggers.size() >= m_capacity);
}
/////////////////////////////////////////////////////////////////////////////
void TriggerEvent::print_triggers()
{
    int n_at = 0;
    int n_tot = (int)m_triggers.size();
    for(auto trig : m_triggers) {
        n_at += 1;
        std::cout << "TriggerEvent::print_triggers    ["
            << n_at << "/" << n_tot << "] " << trig << " ("
                << std::hex << trig << std::dec << ")" << std::endl;
    }
}
/////////////////////////////////////////////////////////////////////////////
uint32_t TriggerEvent::trigger_to_read(bool& ok)
{
    if(!(m_triggers.size()>0)) {
        ok = false;
        return 0xfafafafa;
    }
    else {
        ok = true;
        std::sort(m_triggers.begin(), m_triggers.end());
        return m_triggers.at(0);
    }
}
/////////////////////////////////////////////////////////////////////////////
std::vector<VMMHit> TriggerEvent::get_trigger(uint32_t trigger)
{
    std::vector<VMMHit> empty;
    try {
        return m_trigger_map[trigger];
    } catch(std::exception &e) {
        cout << "TriggerEvent::get_trigger   ERROR " << e.what() << endl;
    }
    return empty;
}
/////////////////////////////////////////////////////////////////////////////
std::map<BoardChipPair, std::vector<VMMHit> > TriggerEvent::get_trigger2(uint32_t trigger)
{
    std::map<BoardChipPair, std::vector<VMMHit> > empty;
    try {
        return m_trigger_map2[trigger];
    }
    catch (std::exception &e) {
        cout << "TriggerEvent::get_trigger2   ERROR " << e.what() << endl;
    }
    return empty;
}
/////////////////////////////////////////////////////////////////////////////
bool TriggerEvent::add_hit(uint32_t trigger, VMMHit hit)
{
    bool ok = true;
    try {
        m_trigger_map[trigger].push_back(hit);
        bool trigger_present = (std::find(m_triggers.begin(), m_triggers.end(), trigger) != m_triggers.end());
        if(!trigger_present) {
            m_triggers.push_back(trigger);
        }
    }
    catch(std::exception &e) {
        cout << "TriggerEvent::add_hit    ERROR adding hit : " << e.what() << endl;
        ok = false;
    }
    return ok;
}
/////////////////////////////////////////////////////////////////////////////
bool TriggerEvent::add_hit2(uint32_t trigger, int32_t board_id, int32_t chip_id, VMMHit hit)
{
    bool ok = true;
    try {
        BoardChipPair pair(board_id, chip_id);
        m_trigger_map2[trigger][pair].push_back(hit);
        bool trigger_present = (std::find(m_triggers.begin(), m_triggers.end(), trigger) != m_triggers.end());
        if(!trigger_present) {
            m_triggers.push_back(trigger);
        }
    }
    catch(std::exception &e) {
        cout << "TriggerEvent::add_hit2    ERROR adding hit : " << e.what() << endl;
        ok = false;
    }
    return ok;

}   
/////////////////////////////////////////////////////////////////////////////
bool TriggerEvent::remove_first_trigger()
{
    bool ok = true;
    try {
        uint32_t first_trigger = m_triggers.at(0);
        remove_trigger(first_trigger);
    }
    catch(std::exception &e) {
        cout << "TriggerEvent::remove_first_trigger    ERROR removing first trigger" << endl;
        ok = false;
    }
    return ok;
}
/////////////////////////////////////////////////////////////////////////////
bool TriggerEvent::remove_trigger(uint32_t trigger_to_remove)
{
    bool ok = true;
    try {
        m_trigger_map.erase(trigger_to_remove);
        m_trigger_map2.erase(trigger_to_remove);
        m_triggers.erase(std::remove(m_triggers.begin(), m_triggers.end(), trigger_to_remove), m_triggers.end());

    }
    catch(std::exception &e) {
        cout << "TriggerEvent::remove_trigger    ERROR removing events for "
            << "trigger #" << trigger_to_remove << endl;
        ok = false;
    }
    return ok;
}

