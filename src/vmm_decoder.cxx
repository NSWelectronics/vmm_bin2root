#include "vmm_decoder.hh"
#include "bit_manip.hh"

// vmm

// std/stl
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <iomanip>
#include <bitset>
using namespace std;

// boost

///////////////////////////////////////////////////////////////////////////////
VMMDecoder::VMMDecoder() :
    m_dbg(false),
    m_readout_type(VMMDecoder::INVALID)
{
    clear();
}
///////////////////////////////////////////////////////////////////////////////
void VMMDecoder::clear(bool clear_header_info)
{
    m_frame = 0;

    m_vmm_channel = 0;
    m_pdo = 0;
    m_tdo = 0;
    if(type()!=VMMDecoder::VMMEVENTL0) {
        m_flag = 0;
        m_bcid = 0;
        m_decoded_bcid = 0;
        m_vmm_overflow = 0;
        m_vmm_parity = 0;
        m_orbit_counter = 0;
    }
    m_pass_threshold = 0;
    m_xadc_samples.clear();

    m_parity = 0;
    m_truncation = 0;
    m_neighbor = 0;
    m_rel_bcid = 0;


    if(clear_header_info) clear_vmm_header_info();
}
///////////////////////////////////////////////////////////////////////////////
void VMMDecoder::clear_vmm_header_info()
{
    m_trigger_counter = 0;
    m_trigger_timestamp = 0;
    m_precision_counter = 0;
    m_chip_number = 0;
    if(type()==VMMDecoder::VMMEVENTL0) {
        m_flag = 0;
        m_bcid = 0;
        m_decoded_bcid = 0;
        m_vmm_overflow = 0;
        m_vmm_parity = 0;
        m_orbit_counter = 0;
    }
    m_art_valid = 0;
    m_art_address = -1;
}
///////////////////////////////////////////////////////////////////////////////
bool VMMDecoder::set_type(int type)
{
    bool ok = true;
    int type_to_set = VMMDecoder::INVALID;

    if(type == VMMDecoder::VMMEVENT)        type_to_set = VMMDecoder::VMMEVENT;
    else if(type == VMMDecoder::VMMEVENTL0) type_to_set = VMMDecoder::VMMEVENTL0;
    else if(type == VMMDecoder::VMMXADC)    type_to_set = VMMDecoder::VMMXADC;
    else {
        cout << "VMMDecoder::set_type    Invalid readout type presented, type set to VMMDecoder:INVALID" << endl;
        ok = false;
    }
    m_readout_type = type_to_set;

    if(m_dbg) {
        cout << "VMMDecoder::set_type    Decoding set: " << type_str() << endl;
    }

    return ok;
}
///////////////////////////////////////////////////////////////////////////////
std::string VMMDecoder::type_str()
{
    std::string typestr = "VMMDecoder::INVALID";
    if(type() == VMMDecoder::VMMEVENT)      typestr = "VMMDecoder::VMMEVENT";
    else if(type() == VMMDecoder::VMMEVENTL0) typestr = "VMMDecoder::VMMEVENTL0";
    else if(type() == VMMDecoder::VMMXADC)  typestr = "VMMDecoder::VMMXADC"; 
    else {
        cout << "VMMDecoder::type_str    Invalid readout type presented" << endl;
    }
    return typestr;
}
///////////////////////////////////////////////////////////////////////////////
bool VMMDecoder::decode(std::vector<uint32_t> datagram, bool is_header)
{
    bool ok = true;
    if(type()==VMMDecoder::VMMEVENT)
        ok = decode_vmm_event_data(datagram, is_header);
    else if(type()==VMMDecoder::VMMEVENTL0)
        ok = decode_vmm_event_data_l0(datagram, is_header);
    else if(type()==VMMDecoder::VMMXADC)
        ok = decode_vmm_xadc_data(datagram);
    else {
        cout << "VMMDecoder::decode    ERROR Invalid readout type presented, unable to decode data!" << endl;
        ok = false;
    }
    return ok;
}
///////////////////////////////////////////////////////////////////////////////
bool VMMDecoder::decode_vmm_event_header(std::vector<uint32_t> datagram)
{
    bool ok = true;

    uint32_t art = datagram.at(0);
    uint32_t d0 = datagram.at(1);
    uint32_t d1 = datagram.at(2);

    // ART
    //cout << "VMMDecoder::decode_vmm_event_header   art header: " << std::hex << art << "  " << (( 0xff00 & art ) >> 8) << endl;
    m_art_valid = ( 0xff00 & art ) >> 8;
    m_art_address = ( 0xff & art );

    // frame (trailer)
    m_frame = d0;


    // trigger counter
    m_trigger_counter  = d0;

    // precision counter
    m_precision_counter = ( d1 & 0xff000000 ) >> 24;

    // trigger timestamp
    m_trigger_timestamp = ( d1 & 0xffff00 ) >> 8;

    // VMM id
    m_chip_number = ( d1 & 0xff );

    if(m_frame == 0xffffffff) { clear(true); ok = false; }

    //if(!(m_frame == 0xffffffff)) {
    //    cout << "VMMDecoder::decode_vmm_event_header    trigger: " << std::hex << m_trigger_counter << "  chip: " << m_chip_number
    //            << "  art_valid: " << m_art_valid << "  art_address: " << m_art_address << endl;
    //} 

    return ok;
}
///////////////////////////////////////////////////////////////////////////////
bool VMMDecoder::decode_vmm_event_header_l0(std::vector<uint32_t> datagram)
{

    //if(datagram.size()!=2) {
    //    cout << "VMMDecoder::decode_vmm_event_header_l0    ERROR Input datagram for L0 header is "
    //        << "not 64 bits wide (size = " << datagram.size() << ")!" << endl;
    //    return false;
    //}

    bool ok = true;

    // ART
    uint32_t art = datagram.at(0);
    m_art_valid = ( 0xff00 & art ) >> 8;
    m_art_address = ( 0xff & art );

    uint32_t d0 = datagram.at(1);
    //d0 = bits::reverse_32()(d0);
    uint32_t d1 = datagram.at(2);
    //d1 = bits::reverse_32()(d1);

    // frame (trailer)
    m_frame = d0;

    // grab the header appended by the FPGA

    // trigger counter
    m_trigger_counter = d0;

    // VMM id
    m_chip_number = ( d1 & 0xff000000 ) >> 24;

    // grab the header from the VMM itself

    // overflow 'V' bit
    m_vmm_overflow = ( d1 & 0x8000 ) >> 15;

    // parity bit
    m_vmm_parity = ( d1 & 0x4000 ) >> 14;

    // orbit counter
    m_orbit_counter = ( d1 & 0x3000 ) >> 12;

    // bcid
    m_bcid = ( d1 & 0xfff );

    // gray decoded bcid
    m_decoded_bcid = VMMDecoder::decode_gray(m_bcid);

    if(m_frame == 0xffffffff) { clear(true); ok = false; }

    return ok;
}
///////////////////////////////////////////////////////////////////////////////
bool VMMDecoder::decode_vmm_event_data(std::vector<uint32_t> datagram, bool is_header)
{
    bool ok = true;
    clear(is_header);

    if(datagram.size()!=2 && !is_header) {
        cout << "VMMDecoder::decode_vmm_event_data    ERROR Input datagram is larger than 8 bytes (size = " << datagram.size() << "), cannot "
            << "decode as VMM event data!" << endl;
        return false;
    }

    if(is_header) {
        return decode_vmm_event_header(datagram);
    }

    uint32_t d0 = datagram.at(0);
    uint32_t d1 = datagram.at(1);

    // frame
    m_frame = d0;


    // pdo
    m_pdo = ( d0 & 0x3ff );

    // gray code (bcid)
    m_bcid = ( d0 & 0x3ffc00) >> 10;

    // decoded gray (bcid)
    m_decoded_bcid = VMMDecoder::decode_gray(m_bcid); 

    // tdo
    m_tdo = ( d0 & 0x3fc00000) >> 22;

    // flag
    m_flag = ( d1 & 0x1);

    // threshold
    m_pass_threshold = ( d1 & 0x2 ) >> 1;

    // vmm channel
    m_vmm_channel = ( d1 & 0xfc ) >> 2;

    if(m_frame == 0xffffffff) { clear(); ok = false; }

    return ok;
}
///////////////////////////////////////////////////////////////////////////////
bool VMMDecoder::decode_vmm_event_data_l0(std::vector<uint32_t> datagram, bool is_header)
{
    bool ok = true;
    clear(is_header);

    // L0 header is 2x32 bits wide
    if(is_header) {
        return decode_vmm_event_header_l0(datagram);
    }

    // L0 hit data is 32 bits wide
    if(datagram.size()!=1) {
        cout << "VMMDecoder::decode_event_data_l0    ERROR Input datagram is not 32-bits "
            << "(size = " << datagram.size() << "), cannot decode as VMM event data!" << endl;
        return false;
    }

    uint32_t d0 = datagram.at(0);
    //d0 = bits::reverse_octet_order()(d0);
    //d0 = bits::endian_swap32()(d0);
    //d0 = bits::reverse_32()(d0);
    //d1 = bits::reverse_32()(d1);

    // frame
    m_frame = d0;

    // parity bit
    //m_parity = ( d0 & 0x2 ) >> 1;
    m_parity = ( d0 & 0x40000000 ) >> 30;

    // truncation bit
    m_truncation = ( d0 & 0x10000000 ) >> 28;
    //m_truncation = ( d0 & 0x8 ) >> 3;

    // VMM channel
    m_vmm_channel = ( d0 & 0xfc00000 ) >> 22;

    // pdo
    m_pdo = ( d0 & 0x3ff000 ) >> 12;
    //m_pdo = ( d0 & 0xffc00 ) >> 10;

    // tdo
    m_tdo = ( d0 & 0xff0 ) >> 4;
    //m_tdo = ( d0 & 0xff00000 ) >> 20;

    // neighbor bit
    m_neighbor = ( d0 & 0x8 ) >> 3;
    //m_neighbor = ( d0 & 0x10000000 ) >> 28;

    // relative bcid
    m_rel_bcid = ( d0 & 0x7 );
    //m_rel_bcid = ( d0 & 0xe0000000 ) >> 29;

    //if(m_frame!=0xffffffff) {
    //cout << "decoder hit data : " << bitset<32>(d0) << endl;
    //cout << "decoder hit data :                 " << std::hex << d0 << std::dec << endl;
    //cout << "decoder hit data :                " << std::bitset<32>(d0) << std::dec << endl;
    //cout << "  > parity: " << m_parity << "  "
    //        << "trunc: " << m_truncation << "  "
    //        << "channel: " << m_vmm_channel << " (" << std::bitset<32>(m_vmm_channel) << ")   "
    //        << "pdo: " << m_pdo << " (" << std::bitset<32>(m_pdo) << ")   " 
    //        << "tdo: " << m_tdo << "  "
    //        << "neighbor: " << m_neighbor << "  "
    //        << "relbcid: " << m_rel_bcid << endl;
    //}

    if(m_frame == 0xffffffff) { clear(); ok = false; }

    return ok;
}
///////////////////////////////////////////////////////////////////////////////
uint32_t VMMDecoder::decode_gray(uint32_t gray)
{
    uint32_t mask;
    for( mask = gray >> 1; mask !=0; mask = mask >> 1) {
        gray = gray ^ mask;
    } // mask
    return gray;

}
///////////////////////////////////////////////////////////////////////////////
bool VMMDecoder::decode_vmm_xadc_data(std::vector<uint32_t> datagram)
{
    clear();
    bool ok = true;

    if(datagram.size()!=2) {
        cout << "VMMDecoder::decode_vmm_xadc_data    ERROR Input datagram is larger than 8 bytes, cannot "
            << "decode as VMM xadc data!" << endl;
        return false;
    }

    uint32_t d0 = datagram.at(0);
    uint32_t d1 = datagram.at(1);

    // frame (trailer)
    uint32_t m_frame = d0;
    if(m_frame == 0xffffffff) { clear(); return false; }

    // vmm id
    m_chip_number = ( d0 & 0xf0 ) >> 4;

    uint32_t s0 = ( (d0 & 0xf) << 8         | (d0 & 0xff00) >> 8 ); 
    uint32_t s1 = ( (d0 & 0xf0000000) >> 28 | (d0 & 0xff0000) >> 12 );
    uint32_t s2 = ( (d0 & 0xf000000) >> 16  | (d1 & 0xff) );
    uint32_t s3 = ( (d1 & 0xff00) >> 4      | (d1 & 0xf00000) >> 20 );
    uint32_t s4 = ( (d1 & 0xff000000) >> 24 | (d1 & 0xf0000) >> 8 );

    m_xadc_samples.push_back(s0);
    m_xadc_samples.push_back(s1);
    m_xadc_samples.push_back(s2);
    m_xadc_samples.push_back(s3);
    m_xadc_samples.push_back(s4);

    if(m_xadc_samples.size() != 5) {
        cout << "VMMDecoder::decode_vmm_xadc_data    ERROR More than 5 xADC samples currently stored!" << endl;
        clear();
        return false;
    }

    return ok;
}
