#ifndef VMM_CONVERTER_HH
#define VMM_CONVERTER_HH

/////////////////////////////////////////////////////////
//
// vmm_converter
//
// main tool for converting binary data file to ROOT
// analysis ntuples
//
// daniel.joseph.antrim@cern.ch
// May 2017
//
/////////////////////////////////////////////////////////

// std/stl
#include <string>
#include <fstream> // ifstream

// vmm
#include "vmm_decoder.hh"
#include "trigger_event.hh"

// ROOT
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TBranch.h"


class VMMConverter
{

    public :
        VMMConverter();
        virtual ~VMMConverter(){};

        void set_debug(int level) { m_dbg = level; }
        void set_plotting(bool doit) { m_do_plots = doit; }

        int dbg() { return m_dbg; }
        bool do_plots() { return m_do_plots; }

        void do_l0_decoding(bool doit);
        bool do_l0() { return m_do_l0_decoding; }

        bool load_file(std::string input_file);
        void setup_output_file();
        void get_run_properties();
        void convert();

        void clear_data();
        bool get_hit_data();
        void fill_trigger_event();
        void fill_event();
        void flush();
        void write_output();

        // helper methods
        float float_from_int(uint32_t input);


        ////////////////////////////////////////////
        // data
        ////////////////////////////////////////////

        // run properties
        uint32_t m_run_number;
        uint32_t m_gain;
        uint32_t m_tac;
        uint32_t m_peak_time;
        uint32_t m_threshold_dac;
        uint32_t m_pulser_dac;
        uint32_t m_angle;
        uint32_t m_tp_skew;

        ///////////////////////////////////////////
        // ROOT NTUPLE
        ///////////////////////////////////////////

        TFile* m_root_file;

        TTree* m_vmm_tree;
        TTree* m_run_properties_tree;

        // run properties

        TBranch* br_run_number;
        TBranch* br_gain;
        TBranch* br_tac;
        TBranch* br_peak_time;
        TBranch* br_threshold_dac;
        TBranch* br_pulser_dac;
        TBranch* br_angle;
        TBranch* br_tp_skew;

        // vmm data
        TBranch* br_trigger_timestamp;
        TBranch* br_trigger_counter;
        TBranch* br_board_id;
        TBranch* br_chip_id;
        TBranch* br_event_size;
        TBranch* br_tdo;
        TBranch* br_pdo;
        TBranch* br_flag;
        TBranch* br_threshold;
        TBranch* br_bcid;
        TBranch* br_gray_decoded;
        TBranch* br_channel;

        // L0
        TBranch* br_relbcid;
        TBranch* br_overflow;
        TBranch* br_orbit_count; 

        std::vector<int> m_board_id;
        std::vector<int> m_trigger_timestamp;
        std::vector<int> m_trigger_counter;
        std::vector<int> m_chip;
        std::vector<int> m_event_size;

        std::vector< std::vector<int> > m_pdo;
        std::vector< std::vector<int> > m_tdo;
        std::vector< std::vector<int> > m_flag;
        std::vector< std::vector<int> > m_threshold;
        std::vector< std::vector<int> > m_bcid;
        std::vector< std::vector<int> > m_gray_decoded;
        std::vector< std::vector<int> > m_channel;
        std::vector< std::vector<int> > m_relbcid; // L0
        std::vector< std::vector<int> > m_overflow; // L0
        std::vector< std::vector<int> > m_orbit_count; // L0
        

    private :
        int m_dbg; // debug level
        bool m_do_l0_decoding; // perform L0 decoding
        bool m_do_plots; // make plots

        std::string m_in_filename; // name of input file
        std::string m_out_filename; // name of output file

        std::ifstream m_ifile;

        VMMDecoder m_decoder;
        TriggerEvent m_trigger_events;

        // counters
        int n_trig_count;


}; // class



#endif
