#ifndef VMM_DECODER_H
#define VMM_DECODER_H

//////////////////////////////////////////////////////////
//
// vmm_decoder
//
// tool to decode the packets from the VMM
//
// daniel.joseph.antrim@cern.ch
// March 2017
//
//////////////////////////////////////////////////////////

// std/stl
#include <vector>
#include <string>
#include <cstdint>


class VMMDecoder
{
    public :
        VMMDecoder();
        virtual ~VMMDecoder(){};

        void set_debug(bool doit) { m_dbg = doit; }
        void clear(bool clear_headers=false);
        void clear_vmm_header_info();

        bool set_type(int in_type);
        int type() { return m_readout_type; }
        std::string type_str();

        bool decode(std::vector<uint32_t> datagram, bool is_header=false);

        enum {
            VMMEVENT=0
            ,VMMEVENTL0
            ,VMMXADC
            ,INVALID
        };

        uint32_t frame() { return m_frame; }

        ///////////////////////////////////////////////
        // grab VMM event data
        ///////////////////////////////////////////////
        uint32_t trigger_count() { return m_trigger_counter; }
        uint32_t precision_count() { return m_precision_counter; }
        uint32_t trigger_timestamp() { return m_trigger_timestamp; }
        uint32_t chip_number() { return m_chip_number; }
        uint32_t vmm_channel() { return m_vmm_channel; }

        uint32_t pdo() { return m_pdo; }
        uint32_t tdo() { return m_tdo; }
        uint32_t bcid() { return m_bcid; }
        uint32_t decoded_bcid() { return m_decoded_bcid; }
        uint32_t flag() { return m_flag; }
        uint32_t pass_threshold() { return m_pass_threshold; }

        // L0
        uint32_t parity_bit() { return m_parity; }
        uint32_t truncation_bit() { return m_truncation; }
        uint32_t neighbor_bit() { return m_neighbor; }
        uint32_t relbcid() { return m_rel_bcid; }
        uint32_t vmm_overflow() { return m_vmm_overflow; }
        uint32_t vmm_parity() { return m_vmm_parity; }
        uint32_t orbit_counter() { return m_orbit_counter; }

        // ART
        uint32_t art_valid() { return m_art_valid; }
        int32_t art_address() { return m_art_address; }

        ///////////////////////////////////////////////
        // grab VMM xADC data
        ///////////////////////////////////////////////
        std::vector<uint32_t> xadc_samples() { return m_xadc_samples; }

    private :
        bool m_dbg;
        int m_readout_type;

        bool decode_vmm_event_data(std::vector<uint32_t> datagram, bool is_header=false);
        bool decode_vmm_event_data_l0(std::vector<uint32_t> datagram, bool is_header=false);
        bool decode_vmm_event_header(std::vector<uint32_t> datagram);
        bool decode_vmm_event_header_l0(std::vector<uint32_t> datagram);
        bool decode_vmm_xadc_data(std::vector<uint32_t> datagram);
        uint32_t decode_gray(uint32_t gray_code);

        uint32_t m_frame;

        ///////////////////////////////////////////////
        // VMM event data
        ///////////////////////////////////////////////
        uint32_t m_trigger_counter;
        uint32_t m_trigger_timestamp;
        uint32_t m_precision_counter;
        uint32_t m_chip_number;
        uint32_t m_vmm_channel;

        uint32_t m_pdo;
        uint32_t m_tdo;
        uint32_t m_bcid;
        uint32_t m_decoded_bcid;
        uint32_t m_flag;
        uint32_t m_pass_threshold;

        // L0 specific
        uint32_t m_parity;
        uint32_t m_truncation;
        uint32_t m_neighbor;
        uint32_t m_rel_bcid;
        uint32_t m_vmm_overflow;
        uint32_t m_vmm_parity;
        uint32_t m_orbit_counter;

        // ART
        uint32_t m_art_valid;
        int32_t m_art_address;

        ///////////////////////////////////////////////
        // VMM xADC samples
        ///////////////////////////////////////////////
        std::vector<uint32_t> m_xadc_samples;


}; // class VMMDecoder

#endif
