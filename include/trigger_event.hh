#ifndef TRIGGER_EVENT_H
#define TRIGGER_EVENT_H

/////////////////////////////////////////
//
// trigger_event
//
// Structure to hold events ordered by
// by the global trigger count
//
// daniel.joseph.antrim@cern.ch
// February 2017
//
/////////////////////////////////////////

// nsw
#include "vmm_hit.hh"

// std/stl
#include <map>
#include <vector>
#include <cstddef>

typedef std::pair<int32_t, int32_t> BoardChipPair;

class TriggerEvent
{
    public :
        TriggerEvent();
        virtual ~TriggerEvent(){};

        

        // clear the contents of the trigger map
        void clear();

        // how many trigger events are currently in the map
        std::size_t size() { return m_trigger_map.size(); }
        std::size_t n_triggers() { return m_triggers.size(); }

        bool empty() { return (m_trigger_map.size()==0); }
        std::size_t capacity() { return m_capacity; }

        bool ready_to_read();

        uint32_t trigger_to_read(bool& ok);

        bool add_hit(uint32_t trigger, VMMHit hit);
        bool add_hit2(uint32_t trigger, int32_t board_id, int32_t chip_id, VMMHit hit);

        std::vector<VMMHit> get_trigger(uint32_t trigger);
        std::map<BoardChipPair, std::vector<VMMHit> > get_trigger2(uint32_t trigger);

        bool remove_trigger(uint32_t trigger_to_remove);

        bool remove_first_trigger();

        void print_triggers();

    private :

        // first index: trigger count
        // second idx : hits associated with that trigger
        std::map<uint32_t, std::vector<VMMHit> > m_trigger_map;
        std::map<uint32_t, std::map<BoardChipPair, std::vector<VMMHit> > > m_trigger_map2;
        std::vector<uint32_t> m_triggers;

        std::size_t m_capacity;
        int n_warning;
};


#endif
