#ifndef VMM_HIT_H
#define VMM_HIT_H

//////////////////////////////////////////
//
// vmm_hit
//
// Structure to hold the VMM hit info
//
// daniel.joseph.antrim@cern.ch
// February 2017
//
//////////////////////////////////////////

// std/stl
#include <stdint.h>

class VMMHit
{

    public :
        VMMHit();
        virtual ~VMMHit(){};

        /////////////////////////////////
        // good ol' getters
        /////////////////////////////////
        uint32_t get_flag() { return m_flag; }
        uint32_t get_pdo() { return m_pdo; }
        uint32_t get_tdo() { return m_tdo; }
        uint32_t get_graycode_bcid() { return m_graycode_bcid; }
        uint32_t get_decoded_graycode_bcid() { return m_decoded_graycode_bcid; }
        uint32_t get_relative_bcid() { return m_relbcid; }
        uint32_t get_overflow_bit() { return m_overflow; }
        uint32_t get_orbit_counter() { return m_orbit_counter; }
        uint32_t get_pass_threshold() { return m_pass_threshold; }
        int32_t  get_vmm_channel() { return m_vmm_channel; }
        int32_t  get_mapped_channel() { return m_mapped_channel; }
        int32_t  get_feb_channel() { return m_feb_channel; }
        int32_t  get_board_id() { return m_board_id; }
        int32_t  get_art() { return m_art; }
        uint32_t get_art_valid() { return m_art_valid; }

        /////////////////////////////////
        // sure can set 'em
        /////////////////////////////////
        void set_flag(uint32_t flag) { m_flag = flag; }
        void set_pdo(uint32_t pdo) { m_pdo = pdo; }
        void set_tdo(uint32_t tdo) { m_tdo = tdo; }
        void set_graycode_bcid(uint32_t graycode) { m_graycode_bcid = graycode; }
        void set_decoded_graycode_bcid(uint32_t decoded) { m_decoded_graycode_bcid = decoded; }
        void set_relative_bcid(uint32_t relbcid) { m_relbcid = relbcid; }
        void set_overflow_bit(uint32_t oflow) { m_overflow = oflow; }
        void set_orbit_counter(uint32_t counter) { m_orbit_counter = counter; }
        void set_pass_threshold(uint32_t threshold) { m_pass_threshold = threshold; }
        void set_vmm_channel(int32_t channel) { m_vmm_channel = channel; }
        void set_mapped_channel(int32_t channel) { m_mapped_channel = channel; }
        void set_feb_channel(int32_t channel) { m_feb_channel = channel; }
        void set_board_id(int32_t boardid) { m_board_id = boardid; } 
        void set_art(int32_t art) { m_art = art; }
        void set_art_valid(uint32_t valid) { m_art_valid = valid; }

        void print();
        void clear();

    private :

        /////////////////////////////////
        // VMM hit info
        /////////////////////////////////
        uint32_t m_flag;
        uint32_t m_pdo;
        uint32_t m_tdo;
        uint32_t m_graycode_bcid;
        uint32_t m_decoded_graycode_bcid;
        uint32_t m_relbcid; // L0
        uint32_t m_overflow; // L0
        uint32_t m_orbit_counter; // L0
        uint32_t m_pass_threshold;
        int32_t  m_vmm_channel;
        int32_t  m_mapped_channel;
        int32_t  m_feb_channel;
        int32_t  m_board_id;
        int32_t m_art; // ART
        uint32_t m_art_valid; // ART
        
        

}; // class


#endif
