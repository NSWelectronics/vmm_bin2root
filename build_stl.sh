#!/bin/bash

###################################################################
#
# this script will build the STL collection dictionary
# needed for ROOT to load vector<vector<T>>
#
# rather than rely on ROOT's in-build CMake macros, which don't work,
# I do this here
#
# daniel.joseph.antrim@cern.ch
# August 2017
#
##################################################################


function print_usage {
    echo "----------------------------------------------------"
    echo "build_stl"
    echo ""
    echo "Options:"
    echo "--root6           Set ROOT6 use (rootcling instead of rootcint for dictionary generation)"
    echo "-h|--help         Print this help message"
    echo ""
    echo "Example usage:"
    echo " $ source build_stl.sh [--root6]"
    echo "----------------------------------------------------"
}

function main {

    startdir=${PWD}
    dictfiledir=$startdir/include/

    version="5"
    while test $# -gt 0
    do
        case $1 in
            --root6)
                version="6"
                ;;
            -h)
                print_usage
                return 0
                ;;
            --help)
                print_usage
                return 0
                ;;
            *)
                echo "ERROR Invalid argument: $1"
                return 1
                ;;
        esac
        shift
    done

    execname="rootcint"
    if [[ $version == "6" ]]; then
        execname="rootcling"
    fi

    dictdir="$startdir/dict"
    if [[ -d ${dictdir} ]]; then
        echo "INFO : Removing previously generated dictionary"
        rm -rf $dictdir
        mkdir -p $dictdir
    else
        mkdir -p $dictdir
    fi

    echo "INFO : Placing STL dictionary in ${dictdir}"

    ${execname} -f ${dictdir}/StlDict.cxx -c ${dictfiledir}/Stl.h ${dictfiledir}/LinkDef.h
    if [[ ! -f ${dictdir}/StlDict.cxx ]]; then
        echo "ERROR : expected file (${dictdir}/StlDict.cxx) not found: dictionary generation failed"
        return 1
    fi
    g++ -o ${dictdir}/libStlDict.so ${dictdir}/StlDict.cxx `root-config --cflags --libs` -shared -fPIC
    if [[ ! -f ${dictdir}/libStlDict.so ]]; then
        echo "ERROR : expected binary (${dictdir}/libStlDict.so) not found: dictionary generation failed"
        return 1
    fi
    echo "INFO : Done"
}

main $*

